# Task Description

##  Create Categories Tree View by data from API.

* **API endpoint :** `https://ppr.backmarket.fr/ws/category/tree/`
* **API documentation :** `https://doc.backmarket.fr/`
* **Token :** `c3VwcG9ydEBoYXNodGFnZXMuY29tOmhhc2h0YWdlczIwMTk=`
* **Example of tree view :** https://codepen.io/deammer/pen/jbVegx


## Global Information :  
    * You can use your preferred stack of technologies.
    * Please upload your final script to repository till 23rd of June.
